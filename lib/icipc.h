/* PipeWire AGL Cluster IPC
 *
 * Copyright © 2021 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __ICIPC_H__
#define __ICIPC_H__

#include "protocol.h"
#include "receiver.h"
#include "sender.h"
#include "server.h"
#include "client.h"

#endif
